from tornado.web import RequestHandler, Application
from tornado.ioloop import IOLoop
from pypgdriver.driver import PostgreDriver
import logging
import os
import json

fmt = ('[%(asctime)s]:[%(levelname)s]:[%(name)s]:[%(threadName)s]:'
       '[%(module)s.%(funcName)s:%(lineno)4d]:%(message)s')
logging.basicConfig(level=logging.DEBUG,
                    format=fmt,
                    datefmt='%Y-%m-%d %H:%M:%S')

logger = logging.getLogger(__name__)


def set_env(key, value):
    if not isinstance(value, str):
        value = str(value)
    os.environ[key] = value


set_env("postgreHost", "localhost")
set_env("postgreDBname", "test")
set_env("postgreUser", "test")
set_env("postgrePassword", "test")
set_env("postgrePort", "5432")


class handler(RequestHandler):

    async def prepare(self):
        self.Connection = PostgreDriver()
        fut = await self.Connection.connect_async()

    def on_finish(self):
        self.Connection._close()
        del self.Connection

    async def post(self):
        sql = "select * from test2"
        try:
            datas = await self.Connection.execute_async(sql)
            if datas:
                self.write(json.dumps(datas))
                self.set_status(200)
        except Exception as e:
            self.set_status(500)
            logger.debug(e)
        self.finish()


router = [(r'/handler', handler)]
app = Application(router)
if __name__ == "__main__":
    app.listen(18888)
    IOLoop.current().start()
